use std::net::SocketAddr;
use std::net::IpAddr;

use actix_web::{get, App, HttpResponse, HttpServer};
use mime::APPLICATION_JSON;
use serde::Serialize;

#[derive(Serialize)]
struct Message {
    content: String
}

pub struct Server {
    host: IpAddr,
    port: u16,
}

impl Server {
    pub fn new(host: IpAddr, port: u16) -> Server {
        Server { host, port }
    }

    pub fn start(&self) {
        let socket_address = SocketAddr::new(self.host, self.port);
        println!("Listening on {}", socket_address);
    }
    
    #[get("/home")]
    fn home() -> HttpResponse {
        HttpResponse::Ok()
            .content_type(APPLICATION_JSON.to_string())
            .json(Message {content: String::from("Hello there")})
    }
}