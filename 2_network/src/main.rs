
// https://github.com/graphql-rust/juniper
// https://actix.rs/docs/getting-started

use actix_web::{get, App, HttpServer, Responder};

#[get("/hellothere")]
async fn hello() -> impl Responder {
    "General Kenobi!"
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .service(hello)
    })
    .bind("127.0.0.1:8080")?
    .run()
    .await
}