# RUST SANDBOX

### Windows
```sh
# install
download & run rustup‑init.exe
use Visual Studio Installer
```

```sh
# run
cargo build --bin myproject
.\target\debug\myproject.exe
```

### Linux
```sh
# install
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

```shell
# run
cargo run --bin myproject
```

### Debug
```shell
rustc --version
cargo --version
cargo new my_project
cd my_project
code .
cargo build
cargo run
# update the root Cargo.toml (project name vs directory name)
```


## Server framework : diesel (query), hyper, actix-web

- https://hyper.rs/


- https://blog.knoldus.com/crud-operations-in-rust/
- https://github.com/iamhabbeboy/rest-api-actix-web/blob/master/src/main.rs
- https://blog.logrocket.com/create-backend-api-with-rust-postgres/
- https://medium.com/sean3z/building-a-restful-crud-api-with-rust-1867308352d8
- https://dev.to/deciduously/oops-i-did-it-againi-made-a-rust-web-api-and-it-was-not-that-difficult-3kk8


## Reinforcement learning : 

- https://docs.rs/rsrl/latest/rsrl/

## Code

- https://doc.rust-lang.org/book/ch17-02-trait-objects.html
- https://doc.rust-lang.org/reference/types/numeric.html
- https://rust-unofficial.github.io/patterns/idioms/ctor.html
- https://stackoverflow.com/questions/66799905/how-to-make-some-structs-fields-mandatory-to-fill-and-others-optional-in-rust
- https://doc.rust-lang.org/std/iter/struct.Map.html
- https://users.rust-lang.org/t/rust-shorthand-for-writing-one-line-functions/42105/3
- https://www.programiz.com/rust/vector

```rust
let port = env::var("PORT").expect("Please set port in .env");
```


## Lifetime

https://www.open-std.org/jtc1/sc22/wg21/docs/papers/2022/p2674r0.pdf 

## Socket server

![Alt text](readme_asset/rust_server_python_client.png)