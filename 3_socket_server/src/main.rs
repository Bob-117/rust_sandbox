use std::env;
use std::net::{TcpListener, TcpStream};
use std::io::{self, Read, Write};
use std::time::{SystemTime, UNIX_EPOCH};
//use std::sync::atomic::{AtomicBool, Ordering}; TODO better close


use std::sync::atomic::{AtomicBool, Ordering};

static STOP_FLAG: AtomicBool = AtomicBool::new(false);


fn handle_client(stream:&mut TcpStream, password: &str) {
    let mut buffer = [0; 1024];
    while let Ok(bytes_read) = stream.read(&mut buffer) {
        if bytes_read == 0 {
            break;
        }
        let message = String::from_utf8_lossy(&buffer[..bytes_read]);
        println!("Incoming message: {}", message);

        let response = match message.trim() {
            "info" => send_current_hour(),
            "shutdown" => ask_for_password_and_stop_server_side(),
            "stop" => ask_for_password_and_stop_client_side(stream, password),
            "atomic" => use_atomic_stop_flag(),
            _ => send_hello_from_rust(),
        };

        stream.write(response.as_bytes()).unwrap();
    }
}

fn send_current_hour() -> String {
    let current_time = SystemTime::now().duration_since(UNIX_EPOCH).unwrap();
    let seconds = current_time.as_secs() + 3600 * 2; // huho
    let local_time = seconds % 86400;

    let hours = local_time / 3600;
    let minutes = (local_time % 3600) / 60;
    let seconds = local_time % 60;

    format!("{:02}:{:02}:{:02}", hours, minutes, seconds)
}


fn ask_for_password_and_stop_server_side() -> String {
    let mut input = String::new();
    print!("Enter password to stop the server: ");
    io::stdout().flush().unwrap();
    io::stdin().read_line(&mut input).unwrap();
    if input.trim() == "password" {
        println!("nope");
        std::process::exit(0);
    } else {
        "Incorrect password. Server will continue running.".to_string()
    }
}

fn ask_for_password_and_stop_client_side(stream: &mut TcpStream, password: &str) -> String {
    stream.write(b"Password ?").unwrap();
    let mut buffer = [0; 1024];
    let bytes_read = stream.read(&mut buffer).unwrap();
    let response = String::from_utf8_lossy(&buffer[..bytes_read]).to_string();

    if response.trim() == password {
        println!("Server is stopping");
        std::process::exit(0);
    } else {
        "Incorrect password. Server will continue running.".to_string()
    }
}

fn use_atomic_stop_flag() -> String {
    print!("USE ATOMIC FLAG METHOD");
    STOP_FLAG.store(true, Ordering::SeqCst);
    //String::new() // TODO
    "ATOMIC".to_string()
}

fn send_hello_from_rust() -> String {
    "Hello from Rust!".to_string()
}


fn main() {

    let password = env::var("SERVER_PASSWORD").unwrap_or_else(|_| {
        println!("No password provided. Server will run without password protection.");
        String::new()
    });

    println!("Initailized password : {}", password);

    //let password = env::args().nth(1).expect("No password provided");


    let listener = TcpListener::bind("127.0.0.1:11117").expect("Failed to bind address");
    println!("Server listening on 127.0.0.1:11117");

    for stream in listener.incoming() {

        // TODO WHILE
        // if STOP_FLAG.load(Ordering::SeqCst) {
        //     println!("atomic stop");
        //     break;
        // }
        // else {
        //     println!("aatomic not yet")
        // }

        match stream {
            Ok(mut stream) => {
                let password_clone = password.clone();
                std::thread::spawn(move || { // move = transfert la propriété de 
                    handle_client(&mut stream, &password_clone);
                });
            }
            Err(e) => {
                eprintln!("Error: {}", e);
            }
        }
    }
}
