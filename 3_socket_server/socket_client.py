import socket
from time import sleep

class SocketClient:
    def __init__(self, remote_server):
        self.remote_server = remote_server
        self.client_socket = None

    def __connect(self, other_port=None):
        self.client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        # self.client_socket.bind(('', 1 if not other_port else other_port))
        self.client_socket.connect(self.remote_server)


    def __close(self):
        self.client_socket.close()


    def send_message_old(self, message):
        self.__connect()
        self.client_socket.sendall(message.encode())
        response = self.client_socket.recv(1024)
        print("Received response:", response.decode())
        self.__close()
    
    def send_message(self, message):
        self.__connect()
        print(f'Sending : {message}')
        self.client_socket.sendall(message.encode())
        response = self.client_socket.recv(1024)
        print("Received response:", response.decode())
        if "password" in response.decode().lower():
            password = input("Enter password: ")
            self.client_socket.sendall(password.encode())
            response = self.client_socket.recv(1024)
            print("Received response:", response.decode())
        self.__close()



def user_story():
    remote = ('127.0.0.1', 11117)
    client = SocketClient(remote_server=remote)
    # client.send_message("Hello from Python!")
    client.send_message("x")
    sleep(1)
    client.send_message("info")
    sleep(1)
    client.send_message("y")
    sleep(1)
    client.send_message("stop")
    sleep(1)
    client.send_message("z")
    sleep(1)
    client.send_message("stop")


def huho(w):
    remote = ('127.0.0.1', 11117)
    client = SocketClient(remote_server=remote)
    client.send_message('info')
    client.send_message('ww')
    sleep(1)
    client.send_message('info')
    client.send_message('ww')
    client.send_message(w)

if __name__ == '__main__':
    huho('atomic')