use rand::{thread_rng, Rng};
use rand::distributions::Alphanumeric; 

const DHCP_IP_CONST: &str = "3.14.15.92";

/************
 *  DEVICE  *
 ************/
trait Device {
    fn ip_address(&self) -> String;
    fn mac_address(&self) -> String;
}
trait Connect {
    fn connect(&self, _target: String) {}
}

impl<T> Connect for T where T: Device {
    fn connect(&self, target: String) {
        println!("The device {} connected to {}", self.ip_address(), target);
    }
 }

 trait Config {
    fn config(&self) {}
}

 impl<T> Config for T where T: Device {
    fn config(&self) {
        println!("IP : {}\nMac : {}", self.ip_address(), self.mac_address());
    }
 }



/************
 *  SERVER  *
 ************/
 pub struct Server {
    
}

 impl Device for Server {
    fn ip_address(&self) -> String {
        DHCP_IP_CONST.to_string()
    }

    fn mac_address(&self) -> String {
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ".to_string()
    }
 }

 impl Server {
    fn get_new_ip(&self) -> i32{
        let mut rng = rand::thread_rng();
        return rng.gen_range(3..255)
    }
 }

/************
 * COMPUTER *
 ************/
pub struct Computer {
    pub(crate) id: i16,
    pub(crate) ip: String,
}

impl Device for Computer {
    fn ip_address(&self) -> String {
        "default".to_string()
    }

    fn mac_address(&self) -> String {
        //"01123581321345589144233377610"
        let rand_string: String = thread_rng()
        .sample_iter(&Alphanumeric)
        .take(12)
        .map(char::from)
        .collect();
        rand_string
    }
}

impl Computer {
    pub fn ask_for_ip(&mut self, dhcp: Server) {
        self.connect(dhcp.ip_address());
        if dhcp.ip_address() == DHCP_IP_CONST {
            let new_ip = "10.0.117.".to_owned() + &dhcp.get_new_ip().to_string();
            println!("SETTING IP TO {}", new_ip);
        } else {
            println!("cant find dhcp")
        }
    }

    pub fn config(&self) {
        println!("{} - IP : {}\nMac : {}", self.id, self.ip_address(), self.mac_address());
    }

}