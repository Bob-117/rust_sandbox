pub struct Item {
    name: String,
    price: f32,
}

impl Item {
    pub fn new(name: String, price: f32) -> Item {
        Item { name, price }
    }

    pub fn show(&self) {
        println!("Name: {}\nPrice: {}", self.name, self.price);
    }

    pub fn update(&mut self, name: String, price: f32) {
        self.name = name;
        self.price = price;
    }
}