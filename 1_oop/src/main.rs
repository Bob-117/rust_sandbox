mod item;
mod cyber;

use item::Item;
use cyber::Server;
use cyber::Computer;

fn main() {
    /* ITEMS */
    let mut my_item = Item::new(String::from("Python"), 0.0);
    my_item.show();
    my_item.update(String::from("GibsonSG"), 1499.99);
    my_item.show();

    /* COMPUTERS */
    const MY_DHCP: Server = Server {};
    let mut my_first_computer: Computer = Computer {id: 17, ip: "".to_string()};

    my_first_computer.ask_for_ip(MY_DHCP);

    let mut computers:Vec<Computer> = vec![];

    for i in 1..4 {
        computers.push(Computer { id: 100 + i , ip: "".to_string()})
    }
    
    for mut computer in computers.into_iter() {
        computer.config();
        computer.ask_for_ip(MY_DHCP);
        computer.config();
    }
}
                               



